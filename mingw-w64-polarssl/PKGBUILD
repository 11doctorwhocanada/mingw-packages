# $Id$
# Maintainer: Kyle Keen <keenerd@gmail.com>
# Contributor: Mihai Militaru <mihai militaru at xmpp dot ro>
# Contributor: carstene1ns <arch carsten-teibes.de>

_realname=polarssl
pkgname="${MINGW_PACKAGE_PREFIX}-${_realname}"
pkgver=1.3.8
pkgrel=1
pkgdesc="Portable cryptographic and SSL/TLS library"
arch=('any')
url="https://www.polarssl.org/"
license=('GPL2')
source=("https://polarssl.org/code/releases/polarssl-$pkgver-gpl.tgz"
        "programs.makefile.patch")
sha1sums=('82ed8ebcf3dd53621da5395b796fc0917083691d'
          '1e9e7d3dcdd6932b02d6dcabdf45041a3726f1be')
depends=("${MINGW_PACKAGE_PREFIX}-gcc")
options=('staticlibs')

# http://sources.gentoo.org/cgi-bin/viewvc.cgi/gentoo-x86/net-libs/polarssl/files/polarssl-1.3.8-ssl_pthread_server.patch
# https://github.com/alucryd/aur-alucryd/blob/master/personal/polarssl/PKGBUILD
# https://aur.archlinux.org/packages/po/polarssl-git/PKGBUILD

build() {
  cd "${_realname}-$pkgver"
  sed -i 's|//\(#define POLARSSL_THREADING_C\)|\1|' include/polarssl/config.h
  sed -i 's|//\(#define POLARSSL_THREADING_PTHREAD\)|\1|' include/polarssl/config.h
  # enable cert_write
  patch -p1 -d programs -i "$srcdir/programs.makefile.patch"
  LDFLAGS+=" -I../include " make SHARED=1 no_test
}

check() {
  cd "${_realname}-$pkgver"
  make SHARED=1 check
}

package() {
  cd "${_realname}-$pkgver"
  make DESTDIR="$pkgdir${MINGW_PREFIX}" install
}
